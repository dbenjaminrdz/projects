#include "BST.h"
using namespace std;


	 NodeInterface * BST::getRootNode()
	 {
	 	if(_root != NULL)
	 	{
	 		return _root;
	 	}
	 }

	 

	 bool BST::insert(Node* &local_root, int data)
	 {

		if (local_root == NULL) {
			local_root = new Node(data);
			return true;
		} 
		else 
		{
			if (data < local_root->_node_value)
				return insert(local_root->_left_child, data);
			else if (local_root->_node_value < data)
				return insert(local_root->_right_child, data);
			else 
				return false;
		}
	 }


	 bool BST::add(int data)
	 {
	 		return insert(_root, data);
	 }




	bool BST::erase(Node*& local_root, const int &data) 
	{
		if (local_root == NULL) 
		{
		return false;
		} 
		else 
		{
			if (data < local_root->_node_value)
				return erase(local_root->_left_child, data);

			else if (local_root->_node_value < data)
				return erase(local_root->_right_child, data);
			
			else 
			{ // Found item
				Node* old_root = local_root;
				if (local_root->_left_child == NULL) 
				{
					local_root = local_root->_right_child;
				} 
				else if (local_root->_right_child == NULL) 
				{
					local_root = local_root->_left_child;
				} 
				else 
				{
				replace_parent(old_root, old_root->_left_child);
				}
				delete old_root;
				return true;
			}
		}
	}

	void BST::replace_parent(Node* &old_root, Node*& local_root) 
	{
		if (local_root->_right_child != NULL) 
		{
		replace_parent(old_root, local_root->_right_child);
		} 
		else 
		{
		old_root->_node_value = local_root->_node_value;
		old_root = local_root;
		local_root = local_root->_left_child;
		}
	}


	bool BST::remove(int data)
	{
		return erase(_root, data);
	}

	 Node* BST::findPredecesor(Node* root)
	 {
	 	if(root->_left_child->_right_child == NULL)
	 		return root->_left_child;
	 	else
	 	{
	 		root = root->_left_child;
	 		while(root->_right_child != NULL)
	 		{
	 			root = root->_right_child;
	 		}
	 		return root;
	 	}
	 }

	 void BST::clear()
	 {
	 	if(_root != NULL)
	 	{
	 		Node* temp1 = _root->_left_child;
	 		Node* temp2 = _root->_right_child;

	 		delete _root;

	 		_root = temp1;
	 		clear();

	 		_root = temp2;
	 		clear();
	 	}

	 }