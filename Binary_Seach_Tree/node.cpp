#include "node.h"

#include <iostream>



	int Node::getData()
	{	
		return _node_value;
	}

	NodeInterface * Node::getLeftChild()
	{
		return _left_child;
	}

 	NodeInterface * Node::getRightChild()
 	{
 		return _right_child;
 	}

