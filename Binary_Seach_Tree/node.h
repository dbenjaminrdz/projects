#include "NodeInterface.h"
#pragma once

#include <iostream>

class Node : public NodeInterface {
friend class BST;
public:

	Node(int value, Node* parent = NULL) 
	{
		_parent = parent;
		_left_child = NULL;
		_right_child = NULL;
		_node_value = value;
	}

	~Node() {}

	int getData();

	NodeInterface * getLeftChild();

 	NodeInterface * getRightChild();

 private:

 	int _node_value;
 	Node* _parent;
 	Node* _left_child;
 	Node* _right_child; 
};