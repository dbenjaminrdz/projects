//YOU MAY NOT MODIFY THIS DOCUMENT

#pragma once

#include "BSTInterface.h"
#include "node.h"


using namespace std;

class BST : public BSTInterface {
public:
	BST() 
	{
		_root = NULL;
	}
	 ~BST() {}

	 NodeInterface * getRootNode();

	 Node* findPredecesor(Node* root);

	 bool insert(Node* &local_root, int data);

	 bool add(int data);

	 bool erase(Node*& local_root, const int &data);

	 void replace_parent(Node* &old_root, Node*& local_root);

	 bool remove(int data);

	 void clear();


private:
	Node* _root;
	Node* _local_root;
};