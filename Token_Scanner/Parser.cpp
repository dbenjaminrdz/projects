#include "Parser.h"
#include <string>
#include <string.h>
#include <iostream>
#include<limits>

using namespace std;


Parser::Parser()
{
}


Parser::~Parser()
{
}

void Parser::colonDash()
{
	if (file.peek() == '-'){
		this_token.type = COLON_DASH;
		this_token.line = line;
		this_token.tokenvalue = ":-";
		allTokens.push_back(this_token);
		file.get();
	}
	else
	{
		this_token.type = COLON;
		this_token.line = line;
		this_token.tokenvalue = current_token;
		allTokens.push_back(this_token);
	}
}

void Parser::getStringLine()
{
	std::string string_helper;
	current_token = file.get();
	string_helper.push_back('\'');

	while (current_token != '\''){
		string_helper.push_back(current_token);
		current_token = file.get();
		if (current_token == '\xff')
		{
			error_line = line;
			break;
		}
	}

	string_helper.push_back('\'');

	if (error_line == -1)
	{
		this_token.line = line;
		this_token.tokenvalue = string_helper;
		this_token.type = STRING;
		allTokens.push_back(this_token);
	}
}

void Parser::getKeyOrId()
{
	std::string string_helper;

	string_helper.push_back(current_token);
	while (isalpha(file.peek()) || isdigit(file.peek()))
	{
		current_token = file.get();
		string_helper.push_back(current_token);
	}

	if (string_helper == "Schemes")
	{
		this_token.line = line;
		this_token.type = SCHEMES;
		this_token.tokenvalue = string_helper;
		allTokens.push_back(this_token);
	}
	else if (string_helper == "Facts")
	{
		this_token.line = line;
		this_token.type = FACTS;
		this_token.tokenvalue = string_helper;
		allTokens.push_back(this_token);
	}
	else if (string_helper == "Rules")
	{
		this_token.line = line;
		this_token.type = RULES;
		this_token.tokenvalue = string_helper;
		allTokens.push_back(this_token);
	}
	else if (string_helper == "Queries")
	{
		this_token.line = line;
		this_token.type = QUERIES;
		this_token.tokenvalue = string_helper;
		allTokens.push_back(this_token);
	}
	else
	{
		this_token.line = line;
		this_token.type = ID;
		this_token.tokenvalue = string_helper;
		allTokens.push_back(this_token);
	}
}

void Parser::scanDocument(std::string path)
{
	line = 1;
	file.open(path);

	while (current_token = file.get())
	{
		if (error_line != -1)
			break;

		if (current_token == '\n')
		{
			line++;
			continue;
		}



		if(current_token == '\xff') // eof
		{
			this_token.type = END;
			this_token.line = line;
			this_token.tokenvalue = "";
			allTokens.push_back(this_token);
			break;
		}
		
		switch (current_token)
		{
			case ' ':
			{
				continue;
			}
			case '\t':
			{
				continue;
			}
			case ',':
			{
				this_token.line = line;
				this_token.tokenvalue = current_token;
				this_token.type = COMMA;
				allTokens.push_back(this_token);
				break;
			}

			case '.':
			{
				this_token.line = line;
				this_token.tokenvalue = current_token;
				this_token.type = PERIOD;
				allTokens.push_back(this_token);
				break;
			}
			case '?':
			{
				this_token.line = line;
				this_token.tokenvalue = current_token;
				this_token.type = Q_MARK;
				allTokens.push_back(this_token);
				break;
			}
			case '(':
			{
				this_token.line = line;
				this_token.tokenvalue = current_token;
				this_token.type = LEFT_PAREN;
				allTokens.push_back(this_token);
				break;
			}
			case ')':
			{
				this_token.line = line;
				this_token.tokenvalue = current_token;
				this_token.type = RIGHT_PAREN;
				allTokens.push_back(this_token);
				break;
			}
			case ':':
			{
				colonDash();
				break;
			}

			case '#':
			{
				file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				line++;
				break;
			}

			case '\'':
			{
				getStringLine();
				break;
			}

			default:
			{// case if letters - will be SCHEMES, FACTS, RULES, QUERIES, ID, STRING
				if (isdigit(current_token))
				{
					error_line = line;
					file.close();
					return;
				}
				else if (isalpha(current_token))
				{
					getKeyOrId();
				}
				else
				{
					error_line = line;
					file.close();
					return;
				}
			}
		}
	}
}

void Parser::printTokens(string name)
{
	std::string type;
	output.open(name);

	for (int i = 0; i < allTokens.size(); i++)
	{
		switch (allTokens[i].type)
		{
		case COMMA:
			type = "COMMA";
			break;
		case PERIOD:
			type = "PERIOD";
			break;
		case Q_MARK:
			type = "Q_MARK";
			break;
		case LEFT_PAREN:
			type = "LEFT_PAREN";
			break;
		case RIGHT_PAREN:
			type = "RIGHT_PAREN";
			break;
		case COLON:
			type = "COLON";
			break;
		case COLON_DASH:
			type = "COLON_DASH";
			break;
		case SCHEMES:
			type = "SCHEMES";
			break;
		case FACTS:
			type = "FACTS";
			break;
		case RULES:
			type = "RULES";
			break;
		case QUERIES:
			type = "QUERIES";
			break;
		case ID:
			type = "ID";
			break;
		case STRING:
			type = "STRING";
			break;
		case END:
			type = "EOF";
			break;
		}

		cout << "(" << type << "," << "\"" << allTokens[i].tokenvalue << "\"" << "," << allTokens[i].line << ")" << endl;
		output << "(" << type << "," << "\"" << allTokens[i].tokenvalue << "\"" << "," << allTokens[i].line << ")" << endl;
	}
	if (error_line == -1)
	{
		cout << "Total Tokens = " << allTokens.size() << endl;
		output << "Total Tokens = " << allTokens.size() << endl;
	}
	else
	{
		cout << "Input Error on line " << error_line << endl;
		output << "Input Error on line " << error_line << endl;
	}

	output.close();
}
	

