#pragma once
#include <string.h>
#include <string>

using namespace std;


enum tokentype {COMMA, PERIOD, Q_MARK, LEFT_PAREN, RIGHT_PAREN, COLON, COLON_DASH, SCHEMES, FACTS, RULES, QUERIES, ID, STRING, END};
class Token
{
friend class Parser;

public:
	Token();
	~Token();

private:
	int line;
	tokentype type;
	string tokenvalue;

};

