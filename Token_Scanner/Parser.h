#pragma once
#include <string>
#include <vector>
#include "Token.h"
#include <fstream>

using namespace std;

class Parser
{
public:
	Parser();
	~Parser();
	void scanDocument(string path);
	void printTokens(string name);
	void colonDash();
	void getStringLine();
	void getKeyOrId();
	
private:
	char current_token;
	char next_token;
	vector<Token> allTokens;
	int line;
	ifstream file;
	ofstream output;
	Token this_token;
	int error_line = -1;
	vector<char> string_constructor;
};

