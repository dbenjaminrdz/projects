#include <iostream>
#include "Token.h"
#include "Parser.h"
#pragma once

using namespace std;

int main(int argcm, char* argv[])
{

		string file_path;
		file_path = argv[1];

		Parser scanner;
		scanner.scanDocument(file_path);
		scanner.printTokens(argv[2]);

		return 0;

}