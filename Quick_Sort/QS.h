#ifndef QS_H_
#define QS_H_

#include <iostream>
#include <string>
#include <sstream>
#include "QSInterface.h"
using namespace std;



class QS : public QSInterface
{
public:

	QS(){}

	~QS(){clear();}

	void sortAll();
	
	int medianOfThree(int left, int right);

	int partition(int left, int right, int pivotIndex);

	string getArray();
	
	int getSize();

	void addToArray(int value);

	bool createArray(int capacity);

	void clear();

	void quick_sort(int first, int last);

	void switch_index(int i, int j);

	void sort_helper(int left, int right, int pivot);



private: 

	int* array = NULL;
	bool existing_array = false;
	int arr_max_index;
	int size = 0;
	int index = 0;
};

#endif /* QS_H_ */
