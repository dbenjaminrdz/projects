#include "QS.h"


	void QS::quick_sort(int first, int last) 
	{
		// if (last - first > 1) 
		// { 
		// 	int pivot = partition(first, last, pivot);

		// 	quick_sort(first, pivot);

		// 	quick_sort(pivot + 1, last);
		// }
	}


	void QS::sort_helper(int left, int right, int pivotIndex)
	{
		int pivot = medianOfThree(left, pivotIndex);
		int pivot2 = medianOfThree(pivotIndex + 1, right);

		pivot = partition(left, pivotIndex, pivot);
		pivot2 = partition(pivotIndex + 1, right, pivot2);

		if(pivot != -1)
		{
			sort_helper(left, pivotIndex, pivot);
		}
		if(pivot2 != -1)
		{
			sort_helper(pivotIndex+1, right, pivot2);
		}
	}

	void QS::sortAll()
	{
		int pivot = medianOfThree(0, size-1);
		pivot = partition(0, size-1, pivot);
		sort_helper(0, size-1, pivot);
	}
	
	void QS::switch_index(int i, int j)
	{
		int temp = array[j];
		array[j] = array[i];
		array[i] = temp;
	}

	int QS::medianOfThree(int left, int right)
	{

		if(right >= size)
			return -1;
		if(left < 0 || right < 0 || left > right)
			return -1;
		if(right - left < 1)
			return -1;
		
		int mid_index = (left + right)/2;


		while(left > mid_index || mid_index > right)
		{
			if (array[left] > array[mid_index])
				switch_index(left, mid_index);
			if (array[mid_index] > array[right])
				switch_index(mid_index, right);
		}
		return mid_index;
	}

	int QS::partition(int left, int right, int pivotIndex)
	{
		if(left < 0 || left > right || pivotIndex < left || pivotIndex > right || right >= size)
			return -1;
		if(array == NULL)
			return -1;

		switch_index(left, pivotIndex);

		pivotIndex = left;
		int up  = left;
		int down = right;


		do
		{
			while(true)
			{
				if(array[pivotIndex] > array[down])
				{
					switch_index(pivotIndex, down);
					pivotIndex = down;
					break;
				}
				if(down != up)
					down--;
				else
				{
					return pivotIndex;
				}
			}
			while(true)
			{
				if(array[pivotIndex] < array[up])
				{
					switch_index(pivotIndex, up);
					pivotIndex = up;
					break;
				}
				if(up != down)
					up++;
				else
					return pivotIndex;
			}
		}
		while(down != up);

		return pivotIndex;
	}

	string QS::getArray()
	{
		stringstream ss;

		if(size == 0)
			return ss.str();
		else
		{
		
			for(int i = 0; i < size; i++)
			{
				ss << array[i];
				if(i != size - 1)
					ss << ",";
			}
		return ss.str();
		}
	}
	
	int QS::getSize()
	{
		return size;
	}

	void QS::addToArray(int value)
	{
		if(array != NULL)
		{
			if(index <= arr_max_index)
			{
			array[index] = value;
			index++;
			size++;
			}
		}
	}

	bool QS::createArray(int capacity)
	{
		if(existing_array)
		{
			delete[] array;
		}
		if(capacity > 0)
		{
			array = new int[capacity];
			arr_max_index = capacity - 1;
			existing_array = true;
			return true;
		}
		else
			return false;

	}

	void QS::clear()
	{
		size = 0;
		delete[] array;
		array = NULL;
		index = 0;
		existing_array = false;
	}
